# Import flask and template operators
from flask import Flask, render_template, redirect, url_for

# Import SQLAlchemy
from flask_sqlalchemy import SQLAlchemy

# Import Migrate
from flask_migrate import Migrate

# Define the WSGI application object
app = Flask(__name__)

# # Configurations
app.config.from_object('config')
mysql = SQLAlchemy(app)
migrate = Migrate(app, mysql)

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

@app.route('/')
def index():
	return app.send_static_file('index.html')

# Import a module / component using its blueprint handler variable (mod_auth)
from app.mod_auth.controllers import mod_auth as auth_module
from app.mod_home.controllers import mod_home as home_module

# Register blueprint(s)
app.register_blueprint(auth_module)
app.register_blueprint(home_module)