# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_home = Blueprint('home', __name__, url_prefix='/home')

@mod_home.route('/index')
def index():
    return render_template("home/index.html")
