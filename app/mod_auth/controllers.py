# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for, jsonify
# Import the database object from the main app module
from app import mysql
# Import module models (i.e. User)
from app.mod_auth.models import Users
# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_auth = Blueprint('auth', __name__, url_prefix='/auth')

# User registration
@mod_auth.route('/add_user', methods=['POST'])
def userRegistration():
    username = request.get_json()["username"]
    password = Users.set_password(request.get_json()["password"])
    email_address = request.get_json()["email_address"]
    user = Users(username=username, password=password, email_address=email_address)
    
    check_username = Users.query.filter_by(username = user.username).first()
    if(check_username is None):
        curr_session = mysql.session
        try:
            curr_session.add(user)
            curr_session.commit()
            result = True
            message = "Register successfully!"
        except:
            result = False
            curr_session.rollback()
            curr_session.flush()
    else:
        result = False
        message = "Register failed!"
    return jsonify({'result': result, 'message': message})

@mod_auth.route('/login_user', methods=['POST'])
def loginButton():
    req_username = request.get_json()["username"]
    req_password = request.get_json()["password"]
    user = Users.query.filter_by(username = req_username).first()
    
    if user and user.validate_password(req_password):
        session['logged_in'] = True
        result = True
        message = "Login successfully!"
    else:
        result = False
        message = "Login failed!"
    return jsonify({'result': result, 'message': message, 'logged_in': session['logged_in']})

@mod_auth.route('/logout')
def logout():
    session['logged_in'] = False
    session.pop('logged_in')
    logged_out = True
    return jsonify({'result': logged_out})

@mod_auth.route('/')
def homePage():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return "Hello: ", session.get('username')

@mod_auth.route('/check_login', methods=['GET'])
def checkLogin():
    return jsonify({'logged_in', session['logged_in']})

@mod_auth.route('/status')
def status():
    if session.get('logged_in'):
        if session['logged_in']:
            return jsonify({'status': True})
    else:
        return jsonify({'status': False})
