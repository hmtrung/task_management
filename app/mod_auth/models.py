from app import mysql
from werkzeug import check_password_hash, generate_password_hash

## map models
class Users(mysql.Model):
    __tablename__ = 'users'
    user_id = mysql.Column(mysql.Integer, nullable = False, primary_key = True)
    username = mysql.Column(mysql.String(45), nullable = False)
    password = mysql.Column(mysql.String(256), nullable = False)
    email_address = mysql.Column(mysql.String(45), nullable = True)
    admin_role = mysql.Column(mysql.Boolean, nullable = False, default=False)
    
    def __init__(self, username, password, email_address):
        self.username = username
        self.password = password
        self.email_address = email_address

    def __repr__(self):
        return '<Users (%s, %s, %s, %s) >' % (self.username, self.password, self.email_address, self.admin_role)

    def set_password(password):
        return generate_password_hash(password)
    
    def validate_password(self, password):
        return check_password_hash(self.password, password)

    # __tablename__ = 'tasks'
    # task_id = mysql.Column(mysql.Integer, nullable = False, primary_key = True)
    # title = mysql.Column(mysql.String(100), nullable = False)
    # desc = mysql.Column(mysql.String(1000), nullable = False)
    # owner = mysql.Column(mysql.Integer, nullable = True)
    # status = mysql.Column(mysql.String(45), nullable = False)
    # priority = mysql.Column(mysql.String(15), nullable = False)
    
