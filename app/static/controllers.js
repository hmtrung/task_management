angular.module('mainApp').controller('loginController',
  ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {
    $scope.login = function () {

      // initial values
      $scope.error = false;
      $scope.disabled = true;

      // call login from service
      AuthService.login($scope.loginForm.username, $scope.loginForm.password)
        // handle success
        .then(function () {
          $location.path('/');
          $scope.disabled = false;
          $scope.loginForm = {};
        })
        // handle error
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Invalid username and/or password";
          $scope.disabled = false;
          $scope.loginForm = {};
        });

    };

}]);

angular.module('mainApp').controller('logoutController',
  ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {
    console.log("test from logoutController");
    // $scope.logout = function () {

      // call logout from service
      AuthService.logout()
        .then(function () {
          $location.path('/');
        });

    // };

}]);

angular.module('mainApp').controller('registerController',
  ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {

    $scope.register = function () {

      // initial values
      $scope.error = false;
      $scope.disabled = true;

      // call register from service
      AuthService.register(	$scope.registerForm.username,
      						$scope.registerForm.email_address,
                           	$scope.registerForm.password)
        // handle success
        .then(function () {
          $location.path('/login');
          $scope.disabled = false;
          $scope.registerForm = {};
        })
        // handle error
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Something went wrong!";
          $scope.disabled = false;
          $scope.registerForm = {};
        });

    };

}]);

angular.module('mainApp').controller('homeController',
  ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {
      // call register from service
      AuthService.getUserStatus()
        // handle success
        .then(function () {
          if (!AuthService.isLoggedIn()) {
            $scope.user = false;
          }
          else {
            $scope.user = true;
          }
          
        })
        // handle error
        .catch(function() {
          $scope.user = false;
        });
}]);

