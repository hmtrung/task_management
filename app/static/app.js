var mainApp = angular.module("mainApp", ['ngRoute', 'ngCookies']);

mainApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.

    when('/', {
        templateUrl: 'static/auth/home.html',
        // controller: "homeController",
        access: {restricted: true}

    }).

    when('/register', {
        templateUrl: 'static/auth/register.html',
        controller: "registerController",
        access: {restricted: true}
    }).

    when('/login', {
    	templateUrl: 'static/auth/login.html',
    	controller: "loginController",
        access: {restricted: true}
    }).


    when('/welcome', {
        templateUrl: 'static/auth/welcome.html',
    }).
    
    when('/logout', {
        templateUrl: 'static/func/logout.html',
        controller: "logoutController",
        access: {restricted: false}
    });
    
    // otherwise({
    // 	redirectTo: '/'
    // });

}]);

// mainApp.run(function ($scope, $location, $route, AuthService) {
//   $scope.$on('$routeChangeStart', function (event, next, current) {
//     if (AuthService.isLoggedIn() === false) {
//       $location.path('/login');
//       $route.reload();
//     }
//   });
// });
